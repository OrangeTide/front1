# Front-end #1

Example of using a web browser as the front-end of your C application.

[![Build Status](https://travis-ci.com/OrangeTide/Front1.svg?branch=master)](https://travis-ci.com/OrangeTide/front1)

## Requirements

 * webview.h (included). See original [webview project](https://github.com/webview/webview) for latest release.
 * civetweb.c and civetweb.h (included). See original [civetweb project](https://github.com/civetweb/civetweb/) for latest release.

### Linux

Ubuntu / Debian :
```sh
sudo apt-get install libwebkit2gtk-4.0-dev libgtk-3-dev
```
