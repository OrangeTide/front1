all :: front1
clean :: ; $(RM) front1
clean-all :: clean
.PHONY : all clean clean-all
CC = g++
#% : %.c
#	$(COMPILE.cc) $(OUTPUT_OPTION) $<
front1 : front1.c webview.cc backend.c civetweb.c
front1 : PKGS = webkit2gtk-4.0 gtk+-3.0
front1 : CFLAGS = -fpermissive $(shell pkg-config --cflags $(PKGS))
front1 : LDFLAGS = $(shell pkg-config --libs $(PKGS))
front1 : LDLIBS = -ldl
front1 : CPPFLAGS = -DOPENSSL_API_1_1
