#include <stdlib.h>
#define WEBVIEW_HEADER
#include "webview.h"
#include "backend.h"

#ifdef _WIN32
int WINAPI
WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#else
int
main()
#endif
{
	if (backend_init())
		return 1;

	webview_t w = webview_create(0, NULL);
	webview_set_title(w, "Front-End #1");
	webview_set_size(w, 800, 600, WEBVIEW_HINT_NONE);
	webview_navigate(w, "http://127.0.0.1:8080/status");
	webview_run(w);
	webview_destroy(w);

	backend_done();

	return 0;
}
