#include "backend.h"
#include <string.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include "civetweb.h"

#define OK (0)
#define ERR (-1)

#define DOCUMENT_ROOT "."
#define LISTENING_PORTS "8080"

static int
logger(const struct mg_connection *conn, const char *message)
{
	fputs(message, stderr);
}

static int
StatusHandler(struct mg_connection *conn, void *cbdata)
{
	mg_printf(conn, "HTTP/1.1 200 OK\r\n");
	mg_printf(conn, "Content-Type: text/plain\r\n");
	mg_printf(conn, "Connection: close\r\n\r\n");

	mg_printf(conn, "Hello World !\n");

	return 1;
}


int
backend_init(void)
{
	const char *options[] = {
		"document_root", DOCUMENT_ROOT,
		"listening_ports", LISTENING_PORTS,
		"error_log_file", "error.log",
		"request_timeout_ms", "5000",
#ifdef USE_WEBSOCKET
		"websocket_timeout_ms", "300000",
#endif
		"enable_auth_domain_check", "no",
		0, 0,
	};

	struct mg_callbacks callbacks;
	memset(&callbacks, 0, sizeof(callbacks));
	callbacks.log_message = logger;
	struct mg_context *ctx = mg_start(&callbacks, 0, options);
	if (!ctx)
		return ERR;

	mg_set_request_handler(ctx, "/status", StatusHandler, 0);
	// mg_set_websocket_handler(ctx, "/websocket", wsConnectHandler, wsReadyHandler, wsDataHandler, wsCloseHandler);

	return OK;
}

void
backend_done(void)
{
	// mg_stop(ctx);
}
